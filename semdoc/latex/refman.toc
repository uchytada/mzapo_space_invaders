\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}% 
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}% 
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}% 
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Bullet Class Reference}{7}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Constructor \& Destructor Documentation}{8}{subsection.4.1.1}% 
\contentsline {subsubsection}{\numberline {4.1.1.1}Bullet()}{8}{subsubsection.4.1.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Member Function Documentation}{8}{subsection.4.1.2}% 
\contentsline {subsubsection}{\numberline {4.1.2.1}getDirection()}{8}{subsubsection.4.1.2.1}% 
\contentsline {subsubsection}{\numberline {4.1.2.2}getOrigin()}{9}{subsubsection.4.1.2.2}% 
\contentsline {section}{\numberline {4.2}Bunker Class Reference}{9}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Constructor \& Destructor Documentation}{10}{subsection.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.2.1.1}Bunker()}{10}{subsubsection.4.2.1.1}% 
\contentsline {subsection}{\numberline {4.2.2}Member Function Documentation}{10}{subsection.4.2.2}% 
\contentsline {subsubsection}{\numberline {4.2.2.1}hit()}{10}{subsubsection.4.2.2.1}% 
\contentsline {section}{\numberline {4.3}Display Class Reference}{11}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Member Function Documentation}{11}{subsection.4.3.1}% 
\contentsline {subsubsection}{\numberline {4.3.1.1}setFrameBuffer()}{11}{subsubsection.4.3.1.1}% 
\contentsline {section}{\numberline {4.4}Enemy Class Reference}{12}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Constructor \& Destructor Documentation}{13}{subsection.4.4.1}% 
\contentsline {subsubsection}{\numberline {4.4.1.1}Enemy()}{13}{subsubsection.4.4.1.1}% 
\contentsline {subsection}{\numberline {4.4.2}Member Function Documentation}{13}{subsection.4.4.2}% 
\contentsline {subsubsection}{\numberline {4.4.2.1}exploaded()}{13}{subsubsection.4.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.4.2.2}getColumn()}{14}{subsubsection.4.4.2.2}% 
\contentsline {subsubsection}{\numberline {4.4.2.3}getExplosionSprite()}{14}{subsubsection.4.4.2.3}% 
\contentsline {subsubsection}{\numberline {4.4.2.4}getMovingSprite()}{14}{subsubsection.4.4.2.4}% 
\contentsline {subsubsection}{\numberline {4.4.2.5}getScoreGain()}{15}{subsubsection.4.4.2.5}% 
\contentsline {subsubsection}{\numberline {4.4.2.6}getState()}{15}{subsubsection.4.4.2.6}% 
\contentsline {subsubsection}{\numberline {4.4.2.7}setState()}{15}{subsubsection.4.4.2.7}% 
\contentsline {section}{\numberline {4.5}Entity Class Reference}{16}{section.4.5}% 
\contentsline {subsection}{\numberline {4.5.1}Constructor \& Destructor Documentation}{16}{subsection.4.5.1}% 
\contentsline {subsubsection}{\numberline {4.5.1.1}Entity()}{16}{subsubsection.4.5.1.1}% 
\contentsline {subsection}{\numberline {4.5.2}Member Function Documentation}{17}{subsection.4.5.2}% 
\contentsline {subsubsection}{\numberline {4.5.2.1}getSprite()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{17}{subsubsection.4.5.2.1}% 
\contentsline {subsubsection}{\numberline {4.5.2.2}getSprite()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{17}{subsubsection.4.5.2.2}% 
\contentsline {subsubsection}{\numberline {4.5.2.3}getX()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{17}{subsubsection.4.5.2.3}% 
\contentsline {subsubsection}{\numberline {4.5.2.4}getX()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{18}{subsubsection.4.5.2.4}% 
\contentsline {subsubsection}{\numberline {4.5.2.5}getY()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{18}{subsubsection.4.5.2.5}% 
\contentsline {subsubsection}{\numberline {4.5.2.6}getY()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{18}{subsubsection.4.5.2.6}% 
\contentsline {subsubsection}{\numberline {4.5.2.7}setX()}{18}{subsubsection.4.5.2.7}% 
\contentsline {subsubsection}{\numberline {4.5.2.8}setY()}{19}{subsubsection.4.5.2.8}% 
\contentsline {section}{\numberline {4.6}font\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}descriptor\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}t Struct Reference}{19}{section.4.6}% 
\contentsline {section}{\numberline {4.7}Knob Class Reference}{19}{section.4.7}% 
\contentsline {subsection}{\numberline {4.7.1}Constructor \& Destructor Documentation}{20}{subsection.4.7.1}% 
\contentsline {subsubsection}{\numberline {4.7.1.1}Knob()}{20}{subsubsection.4.7.1.1}% 
\contentsline {subsection}{\numberline {4.7.2}Member Function Documentation}{20}{subsection.4.7.2}% 
\contentsline {subsubsection}{\numberline {4.7.2.1}button()}{20}{subsubsection.4.7.2.1}% 
\contentsline {subsubsection}{\numberline {4.7.2.2}ignoreButtonMS()}{20}{subsubsection.4.7.2.2}% 
\contentsline {subsubsection}{\numberline {4.7.2.3}increment()}{21}{subsubsection.4.7.2.3}% 
\contentsline {section}{\numberline {4.8}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}D\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Line Class Reference}{21}{section.4.8}% 
\contentsline {subsection}{\numberline {4.8.1}Constructor \& Destructor Documentation}{21}{subsection.4.8.1}% 
\contentsline {subsubsection}{\numberline {4.8.1.1}LEDLine()}{21}{subsubsection.4.8.1.1}% 
\contentsline {section}{\numberline {4.9}Menu Class Reference}{22}{section.4.9}% 
\contentsline {subsection}{\numberline {4.9.1}Constructor \& Destructor Documentation}{22}{subsection.4.9.1}% 
\contentsline {subsubsection}{\numberline {4.9.1.1}Menu()}{22}{subsubsection.4.9.1.1}% 
\contentsline {subsection}{\numberline {4.9.2}Member Function Documentation}{23}{subsection.4.9.2}% 
\contentsline {subsubsection}{\numberline {4.9.2.1}setOptions()}{23}{subsubsection.4.9.2.1}% 
\contentsline {subsubsection}{\numberline {4.9.2.2}setOptionsKeyboard()}{23}{subsubsection.4.9.2.2}% 
\contentsline {section}{\numberline {4.10}Pixel Class Reference}{24}{section.4.10}% 
\contentsline {subsection}{\numberline {4.10.1}Constructor \& Destructor Documentation}{24}{subsection.4.10.1}% 
\contentsline {subsubsection}{\numberline {4.10.1.1}Pixel()}{24}{subsubsection.4.10.1.1}% 
\contentsline {subsection}{\numberline {4.10.2}Member Function Documentation}{25}{subsection.4.10.2}% 
\contentsline {subsubsection}{\numberline {4.10.2.1}getBlue()}{25}{subsubsection.4.10.2.1}% 
\contentsline {subsubsection}{\numberline {4.10.2.2}getGreen()}{25}{subsubsection.4.10.2.2}% 
\contentsline {subsubsection}{\numberline {4.10.2.3}getRed()}{26}{subsubsection.4.10.2.3}% 
\contentsline {subsubsection}{\numberline {4.10.2.4}setBlue()}{26}{subsubsection.4.10.2.4}% 
\contentsline {subsubsection}{\numberline {4.10.2.5}setGreen()}{26}{subsubsection.4.10.2.5}% 
\contentsline {subsubsection}{\numberline {4.10.2.6}setPixel()}{26}{subsubsection.4.10.2.6}% 
\contentsline {subsubsection}{\numberline {4.10.2.7}setRed()}{27}{subsubsection.4.10.2.7}% 
\contentsline {section}{\numberline {4.11}Player Class Reference}{28}{section.4.11}% 
\contentsline {subsection}{\numberline {4.11.1}Constructor \& Destructor Documentation}{29}{subsection.4.11.1}% 
\contentsline {subsubsection}{\numberline {4.11.1.1}Player()}{29}{subsubsection.4.11.1.1}% 
\contentsline {subsection}{\numberline {4.11.2}Member Function Documentation}{29}{subsection.4.11.2}% 
\contentsline {subsubsection}{\numberline {4.11.2.1}getExplosionSprite()}{29}{subsubsection.4.11.2.1}% 
\contentsline {subsubsection}{\numberline {4.11.2.2}getHealth()}{30}{subsubsection.4.11.2.2}% 
\contentsline {subsubsection}{\numberline {4.11.2.3}getScore()}{30}{subsubsection.4.11.2.3}% 
\contentsline {subsubsection}{\numberline {4.11.2.4}hit()}{30}{subsubsection.4.11.2.4}% 
\contentsline {subsubsection}{\numberline {4.11.2.5}incrementScore()}{31}{subsubsection.4.11.2.5}% 
\contentsline {subsubsection}{\numberline {4.11.2.6}isHit()}{31}{subsubsection.4.11.2.6}% 
\contentsline {subsubsection}{\numberline {4.11.2.7}setHealth()}{32}{subsubsection.4.11.2.7}% 
\contentsline {subsubsection}{\numberline {4.11.2.8}setScore()}{32}{subsubsection.4.11.2.8}% 
\contentsline {section}{\numberline {4.12}R\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}G\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}B\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}ED Class Reference}{32}{section.4.12}% 
\contentsline {subsection}{\numberline {4.12.1}Constructor \& Destructor Documentation}{32}{subsection.4.12.1}% 
\contentsline {subsubsection}{\numberline {4.12.1.1}RGBLED()}{33}{subsubsection.4.12.1.1}% 
\contentsline {subsection}{\numberline {4.12.2}Member Function Documentation}{33}{subsection.4.12.2}% 
\contentsline {subsubsection}{\numberline {4.12.2.1}blink()}{33}{subsubsection.4.12.2.1}% 
\contentsline {subsubsection}{\numberline {4.12.2.2}setColor()}{34}{subsubsection.4.12.2.2}% 
\contentsline {section}{\numberline {4.13}Sprite Class Reference}{34}{section.4.13}% 
\contentsline {subsection}{\numberline {4.13.1}Constructor \& Destructor Documentation}{35}{subsection.4.13.1}% 
\contentsline {subsubsection}{\numberline {4.13.1.1}Sprite()\hspace {0.1cm}{\footnotesize \ttfamily [1/3]}}{35}{subsubsection.4.13.1.1}% 
\contentsline {subsubsection}{\numberline {4.13.1.2}Sprite()\hspace {0.1cm}{\footnotesize \ttfamily [2/3]}}{35}{subsubsection.4.13.1.2}% 
\contentsline {subsubsection}{\numberline {4.13.1.3}Sprite()\hspace {0.1cm}{\footnotesize \ttfamily [3/3]}}{36}{subsubsection.4.13.1.3}% 
\contentsline {subsection}{\numberline {4.13.2}Member Function Documentation}{36}{subsection.4.13.2}% 
\contentsline {subsubsection}{\numberline {4.13.2.1}damage()}{36}{subsubsection.4.13.2.1}% 
\contentsline {subsubsection}{\numberline {4.13.2.2}getHeight()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{36}{subsubsection.4.13.2.2}% 
\contentsline {subsubsection}{\numberline {4.13.2.3}getHeight()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{37}{subsubsection.4.13.2.3}% 
\contentsline {subsubsection}{\numberline {4.13.2.4}getPixel()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{37}{subsubsection.4.13.2.4}% 
\contentsline {subsubsection}{\numberline {4.13.2.5}getPixel()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{37}{subsubsection.4.13.2.5}% 
\contentsline {subsubsection}{\numberline {4.13.2.6}getWidth()\hspace {0.1cm}{\footnotesize \ttfamily [1/2]}}{38}{subsubsection.4.13.2.6}% 
\contentsline {subsubsection}{\numberline {4.13.2.7}getWidth()\hspace {0.1cm}{\footnotesize \ttfamily [2/2]}}{38}{subsubsection.4.13.2.7}% 
\contentsline {subsubsection}{\numberline {4.13.2.8}setColor()}{38}{subsubsection.4.13.2.8}% 
\contentsline {section}{\numberline {4.14}Text Class Reference}{39}{section.4.14}% 
\contentsline {subsection}{\numberline {4.14.1}Constructor \& Destructor Documentation}{39}{subsection.4.14.1}% 
\contentsline {subsubsection}{\numberline {4.14.1.1}Text()}{39}{subsubsection.4.14.1.1}% 
\contentsline {subsection}{\numberline {4.14.2}Member Function Documentation}{39}{subsection.4.14.2}% 
\contentsline {subsubsection}{\numberline {4.14.2.1}drawLine()}{40}{subsubsection.4.14.2.1}% 
\contentsline {chapter}{\numberline {5}File Documentation}{41}{chapter.5}% 
\contentsline {section}{\numberline {5.1}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets.h File Reference}{41}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{42}{subsection.5.1.1}% 
\contentsline {section}{\numberline {5.2}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bullet.h File Reference}{43}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{44}{subsection.5.2.1}% 
\contentsline {section}{\numberline {5.3}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bunker.h File Reference}{44}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Detailed Description}{45}{subsection.5.3.1}% 
\contentsline {section}{\numberline {5.4}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Display.h File Reference}{45}{section.5.4}% 
\contentsline {subsection}{\numberline {5.4.1}Detailed Description}{46}{subsection.5.4.1}% 
\contentsline {section}{\numberline {5.5}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Enemy.h File Reference}{46}{section.5.5}% 
\contentsline {subsection}{\numberline {5.5.1}Detailed Description}{48}{subsection.5.5.1}% 
\contentsline {section}{\numberline {5.6}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Entity.h File Reference}{48}{section.5.6}% 
\contentsline {subsection}{\numberline {5.6.1}Detailed Description}{49}{subsection.5.6.1}% 
\contentsline {section}{\numberline {5.7}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game.h File Reference}{49}{section.5.7}% 
\contentsline {subsection}{\numberline {5.7.1}Detailed Description}{50}{subsection.5.7.1}% 
\contentsline {subsection}{\numberline {5.7.2}Function Documentation}{51}{subsection.5.7.2}% 
\contentsline {subsubsection}{\numberline {5.7.2.1}bunkerHit()}{51}{subsubsection.5.7.2.1}% 
\contentsline {subsubsection}{\numberline {5.7.2.2}clearPixelGrid()}{51}{subsubsection.5.7.2.2}% 
\contentsline {subsubsection}{\numberline {5.7.2.3}drawBunkers()}{51}{subsubsection.5.7.2.3}% 
\contentsline {subsubsection}{\numberline {5.7.2.4}enemyHit()}{52}{subsubsection.5.7.2.4}% 
\contentsline {subsubsection}{\numberline {5.7.2.5}initEnemies()}{52}{subsubsection.5.7.2.5}% 
\contentsline {subsubsection}{\numberline {5.7.2.6}playerHit()}{53}{subsubsection.5.7.2.6}% 
\contentsline {subsubsection}{\numberline {5.7.2.7}resolveBullets()}{53}{subsubsection.5.7.2.7}% 
\contentsline {subsubsection}{\numberline {5.7.2.8}resolveEnemies()}{54}{subsubsection.5.7.2.8}% 
\contentsline {subsubsection}{\numberline {5.7.2.9}resolvePlayer()}{54}{subsubsection.5.7.2.9}% 
\contentsline {section}{\numberline {5.8}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Keyboard.h File Reference}{55}{section.5.8}% 
\contentsline {subsection}{\numberline {5.8.1}Detailed Description}{55}{subsection.5.8.1}% 
\contentsline {subsection}{\numberline {5.8.2}Function Documentation}{55}{subsection.5.8.2}% 
\contentsline {subsubsection}{\numberline {5.8.2.1}call\_termios()}{55}{subsubsection.5.8.2.1}% 
\contentsline {subsubsection}{\numberline {5.8.2.2}io\_getc\_timeout()}{56}{subsubsection.5.8.2.2}% 
\contentsline {subsubsection}{\numberline {5.8.2.3}keyboard()}{56}{subsubsection.5.8.2.3}% 
\contentsline {section}{\numberline {5.9}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Knobs.h File Reference}{57}{section.5.9}% 
\contentsline {subsection}{\numberline {5.9.1}Detailed Description}{57}{subsection.5.9.1}% 
\contentsline {section}{\numberline {5.10}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}D\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Line.h File Reference}{57}{section.5.10}% 
\contentsline {subsection}{\numberline {5.10.1}Detailed Description}{58}{subsection.5.10.1}% 
\contentsline {section}{\numberline {5.11}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Menu.h File Reference}{58}{section.5.11}% 
\contentsline {subsection}{\numberline {5.11.1}Detailed Description}{59}{subsection.5.11.1}% 
\contentsline {section}{\numberline {5.12}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Player.h File Reference}{59}{section.5.12}% 
\contentsline {subsection}{\numberline {5.12.1}Detailed Description}{61}{subsection.5.12.1}% 
\contentsline {section}{\numberline {5.13}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}G\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}B\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Ds.h File Reference}{61}{section.5.13}% 
\contentsline {subsection}{\numberline {5.13.1}Detailed Description}{61}{subsection.5.13.1}% 
\contentsline {section}{\numberline {5.14}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sprite.h File Reference}{62}{section.5.14}% 
\contentsline {subsection}{\numberline {5.14.1}Detailed Description}{63}{subsection.5.14.1}% 
\contentsline {section}{\numberline {5.15}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Text.h File Reference}{63}{section.5.15}% 
\contentsline {subsection}{\numberline {5.15.1}Detailed Description}{64}{subsection.5.15.1}% 
\contentsline {section}{\numberline {5.16}semcode/include/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Time\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stamp.h File Reference}{64}{section.5.16}% 
\contentsline {subsection}{\numberline {5.16.1}Detailed Description}{64}{subsection.5.16.1}% 
\contentsline {subsection}{\numberline {5.16.2}Function Documentation}{65}{subsection.5.16.2}% 
\contentsline {subsubsection}{\numberline {5.16.2.1}currentTimestamp()}{65}{subsubsection.5.16.2.1}% 
\contentsline {section}{\numberline {5.17}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets.cpp File Reference}{65}{section.5.17}% 
\contentsline {subsection}{\numberline {5.17.1}Detailed Description}{66}{subsection.5.17.1}% 
\contentsline {subsection}{\numberline {5.17.2}Variable Documentation}{66}{subsection.5.17.2}% 
\contentsline {subsubsection}{\numberline {5.17.2.1}AlienA}{66}{subsubsection.5.17.2.1}% 
\contentsline {subsubsection}{\numberline {5.17.2.2}AlienAMoving}{66}{subsubsection.5.17.2.2}% 
\contentsline {subsubsection}{\numberline {5.17.2.3}AlienB}{67}{subsubsection.5.17.2.3}% 
\contentsline {subsubsection}{\numberline {5.17.2.4}AlienBMoving}{67}{subsubsection.5.17.2.4}% 
\contentsline {subsubsection}{\numberline {5.17.2.5}AlienC}{67}{subsubsection.5.17.2.5}% 
\contentsline {subsubsection}{\numberline {5.17.2.6}AlienCMoving}{67}{subsubsection.5.17.2.6}% 
\contentsline {subsubsection}{\numberline {5.17.2.7}AlienExplodingSprite}{67}{subsubsection.5.17.2.7}% 
\contentsline {subsubsection}{\numberline {5.17.2.8}AlienShotExploding}{68}{subsubsection.5.17.2.8}% 
\contentsline {subsubsection}{\numberline {5.17.2.9}FlyingSaucerSprite}{68}{subsubsection.5.17.2.9}% 
\contentsline {subsubsection}{\numberline {5.17.2.10}PlayerBlowupSprite1}{68}{subsubsection.5.17.2.10}% 
\contentsline {subsubsection}{\numberline {5.17.2.11}PlayerBlowupSprite2}{68}{subsubsection.5.17.2.11}% 
\contentsline {subsubsection}{\numberline {5.17.2.12}PlayerShotExploding}{69}{subsubsection.5.17.2.12}% 
\contentsline {subsubsection}{\numberline {5.17.2.13}PlayerShotSprite}{69}{subsubsection.5.17.2.13}% 
\contentsline {subsubsection}{\numberline {5.17.2.14}PlayerSprite}{69}{subsubsection.5.17.2.14}% 
\contentsline {subsubsection}{\numberline {5.17.2.15}PlungerShotSpite1}{69}{subsubsection.5.17.2.15}% 
\contentsline {subsubsection}{\numberline {5.17.2.16}PlungerShotSpite2}{69}{subsubsection.5.17.2.16}% 
\contentsline {subsubsection}{\numberline {5.17.2.17}PlungerShotSpite3}{70}{subsubsection.5.17.2.17}% 
\contentsline {subsubsection}{\numberline {5.17.2.18}PlungerShotSpite4}{70}{subsubsection.5.17.2.18}% 
\contentsline {subsubsection}{\numberline {5.17.2.19}RollingShotSprite1}{70}{subsubsection.5.17.2.19}% 
\contentsline {subsubsection}{\numberline {5.17.2.20}RollingShotSprite2}{70}{subsubsection.5.17.2.20}% 
\contentsline {subsubsection}{\numberline {5.17.2.21}RollingShotSprite3}{70}{subsubsection.5.17.2.21}% 
\contentsline {subsubsection}{\numberline {5.17.2.22}ShieldImage}{71}{subsubsection.5.17.2.22}% 
\contentsline {subsubsection}{\numberline {5.17.2.23}SpriteSaucerExp}{71}{subsubsection.5.17.2.23}% 
\contentsline {subsubsection}{\numberline {5.17.2.24}SquiglyShotSprite1}{71}{subsubsection.5.17.2.24}% 
\contentsline {subsubsection}{\numberline {5.17.2.25}SquiglyShotSprite2}{71}{subsubsection.5.17.2.25}% 
\contentsline {subsubsection}{\numberline {5.17.2.26}SquiglyShotSprite3}{71}{subsubsection.5.17.2.26}% 
\contentsline {section}{\numberline {5.18}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bullet.cpp File Reference}{72}{section.5.18}% 
\contentsline {subsection}{\numberline {5.18.1}Detailed Description}{72}{subsection.5.18.1}% 
\contentsline {section}{\numberline {5.19}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Bunker.cpp File Reference}{72}{section.5.19}% 
\contentsline {subsection}{\numberline {5.19.1}Detailed Description}{73}{subsection.5.19.1}% 
\contentsline {section}{\numberline {5.20}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Display.cpp File Reference}{73}{section.5.20}% 
\contentsline {subsection}{\numberline {5.20.1}Detailed Description}{74}{subsection.5.20.1}% 
\contentsline {section}{\numberline {5.21}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Enemy.cpp File Reference}{74}{section.5.21}% 
\contentsline {subsection}{\numberline {5.21.1}Detailed Description}{75}{subsection.5.21.1}% 
\contentsline {section}{\numberline {5.22}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Entity.cpp File Reference}{75}{section.5.22}% 
\contentsline {subsection}{\numberline {5.22.1}Detailed Description}{76}{subsection.5.22.1}% 
\contentsline {section}{\numberline {5.23}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game.cpp File Reference}{76}{section.5.23}% 
\contentsline {subsection}{\numberline {5.23.1}Detailed Description}{78}{subsection.5.23.1}% 
\contentsline {subsection}{\numberline {5.23.2}Function Documentation}{78}{subsection.5.23.2}% 
\contentsline {subsubsection}{\numberline {5.23.2.1}bunkerHit()}{78}{subsubsection.5.23.2.1}% 
\contentsline {subsubsection}{\numberline {5.23.2.2}clearPixelGrid()}{78}{subsubsection.5.23.2.2}% 
\contentsline {subsubsection}{\numberline {5.23.2.3}drawBunkers()}{79}{subsubsection.5.23.2.3}% 
\contentsline {subsubsection}{\numberline {5.23.2.4}enemyHit()}{79}{subsubsection.5.23.2.4}% 
\contentsline {subsubsection}{\numberline {5.23.2.5}initEnemies()}{80}{subsubsection.5.23.2.5}% 
\contentsline {subsubsection}{\numberline {5.23.2.6}playerHit()}{80}{subsubsection.5.23.2.6}% 
\contentsline {subsubsection}{\numberline {5.23.2.7}resolveBullets()}{81}{subsubsection.5.23.2.7}% 
\contentsline {subsubsection}{\numberline {5.23.2.8}resolveEnemies()}{82}{subsubsection.5.23.2.8}% 
\contentsline {subsubsection}{\numberline {5.23.2.9}resolvePlayer()}{82}{subsubsection.5.23.2.9}% 
\contentsline {section}{\numberline {5.24}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Keyboard.cpp File Reference}{83}{section.5.24}% 
\contentsline {subsection}{\numberline {5.24.1}Detailed Description}{83}{subsection.5.24.1}% 
\contentsline {subsection}{\numberline {5.24.2}Function Documentation}{84}{subsection.5.24.2}% 
\contentsline {subsubsection}{\numberline {5.24.2.1}call\_termios()}{84}{subsubsection.5.24.2.1}% 
\contentsline {subsubsection}{\numberline {5.24.2.2}io\_getc\_timeout()}{84}{subsubsection.5.24.2.2}% 
\contentsline {subsubsection}{\numberline {5.24.2.3}keyboard()}{84}{subsubsection.5.24.2.3}% 
\contentsline {section}{\numberline {5.25}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Knobs.cpp File Reference}{85}{section.5.25}% 
\contentsline {subsection}{\numberline {5.25.1}Detailed Description}{85}{subsection.5.25.1}% 
\contentsline {section}{\numberline {5.26}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}D\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Line.cpp File Reference}{85}{section.5.26}% 
\contentsline {subsection}{\numberline {5.26.1}Detailed Description}{86}{subsection.5.26.1}% 
\contentsline {section}{\numberline {5.27}semcode/source/main.cpp File Reference}{86}{section.5.27}% 
\contentsline {subsection}{\numberline {5.27.1}Detailed Description}{87}{subsection.5.27.1}% 
\contentsline {section}{\numberline {5.28}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Menu.cpp File Reference}{87}{section.5.28}% 
\contentsline {subsection}{\numberline {5.28.1}Detailed Description}{88}{subsection.5.28.1}% 
\contentsline {section}{\numberline {5.29}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Player.cpp File Reference}{88}{section.5.29}% 
\contentsline {subsection}{\numberline {5.29.1}Detailed Description}{89}{subsection.5.29.1}% 
\contentsline {section}{\numberline {5.30}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}R\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}G\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}B\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}L\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Ds.cpp File Reference}{89}{section.5.30}% 
\contentsline {subsection}{\numberline {5.30.1}Detailed Description}{90}{subsection.5.30.1}% 
\contentsline {section}{\numberline {5.31}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sprite.cpp File Reference}{90}{section.5.31}% 
\contentsline {subsection}{\numberline {5.31.1}Detailed Description}{91}{subsection.5.31.1}% 
\contentsline {section}{\numberline {5.32}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Text.cpp File Reference}{91}{section.5.32}% 
\contentsline {subsection}{\numberline {5.32.1}Detailed Description}{91}{subsection.5.32.1}% 
\contentsline {section}{\numberline {5.33}semcode/source/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Time\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Stamp.cpp File Reference}{92}{section.5.33}% 
\contentsline {subsection}{\numberline {5.33.1}Detailed Description}{92}{subsection.5.33.1}% 
\contentsline {subsection}{\numberline {5.33.2}Function Documentation}{92}{subsection.5.33.2}% 
\contentsline {subsubsection}{\numberline {5.33.2.1}currentTimestamp()}{92}{subsubsection.5.33.2.1}% 
\contentsline {chapter}{Index}{93}{section*.126}% 
