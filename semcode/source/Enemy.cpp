/// @file
/// @brief Enemy module source file

#include "Enemy.h"
#include "Entity.h"
#include "Sprite.h"

Enemy::Enemy(unsigned int InScoreGain, Sprite InitExpSprite,
             Sprite InitMovingSprite, Sprite InitSprite, const int &InColumn,
             const int &InitY, const int &InitX)
    : Entity(InitSprite, InitY, InitX), ExplosionSprite(InitExpSprite),
      MovingSprite(InitMovingSprite) {
  scoreGain = InScoreGain;
  Exploaded = false;
  ExplosionSprite = InitExpSprite;
  Column = InColumn;
  State = MovingLeft;
}

bool Enemy::exploaded() { return Exploaded; }

EnemyState Enemy::getState() {
  EnemyState Ret = State;
  if (Ret == MovingLeft) {
    if (--Cursor == 0) {
      State = MovingRight;
    }
  } else if (Ret == MovingRight) {
    if (++Cursor == MaxCursor) {
      State = MovingLeft;
    }
  }
  return Ret;
}

void Enemy::setState(const EnemyState &InState) { State = InState; }

int Enemy::getColumn() { return Column; }

Sprite Enemy::getMovingSprite() {
  if (Moving) {
    Moving = !Moving;
    return MovingSprite;
  } else {
    Moving = !Moving;
    return getSprite();
  }
}

void Enemy::setExploded() { Exploaded = true; }

Sprite Enemy::getExplosionSprite() { return ExplosionSprite; }

unsigned int Enemy::getScoreGain() { return scoreGain; }
