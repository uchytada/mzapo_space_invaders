/// @file
/// @brief Display module source file

#include <cstdint>
#include <iostream>
#include <stdexcept>

#include "Display.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

Display::Display() {

  try {
    MemBase = static_cast<uint8_t *>(
        map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0));
    if (MemBase == NULL) {
      throw std::runtime_error("Failed to map physical adress");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }

  parlcd_hx8357_init(MemBase);

  parlcd_write_cmd(MemBase, 0x2c);

  for (int i = 0; i < HEIGHT; ++i) {
    for (int j = 0; j < WIDTH; ++j) {
      parlcd_write_data(MemBase, 0u);
    }
  }

  FrameBuffer = new (uint16_t[HEIGHT * WIDTH]);
}

Display::~Display() { delete[](FrameBuffer); }

void Display::setFrameBuffer(const Grid &Pixels) {
  uint16_t *FrameBufferIterator = FrameBuffer;
  for (auto Column : Pixels) {
    for (auto Element : Column) {
      uint16_t Color = Element.getBlue() | (Element.getGreen() << 6) |
                       (Element.getRed() << 11);
      *(FrameBufferIterator++) = Color;
    }
  }
}

void Display::redraw() {

  parlcd_write_cmd(MemBase, 0x2c);
  uint16_t *FrameBufferIterator = FrameBuffer;
  for (int i = 0; i < HEIGHT; ++i) {
    for (int j = 0; j < WIDTH; ++j) {
      parlcd_write_data(MemBase, (*(FrameBufferIterator++)));
    }
  }
}

Pixel::Pixel() {
  Red = 0;
  Green = 0;
  Blue = 0;
}

Pixel::Pixel(const uint8_t &Red, const uint8_t &Green, const uint8_t &Blue) {
  setPixel(Red, Green, Blue);
}

void Pixel::setPixel(const uint8_t &Red, const uint8_t &Green,
                     const uint8_t &Blue) {

  setRed(Red);
  setGreen(Green);
  setBlue(Blue);
}

uint8_t Pixel::getRed() { return this->Red; }

uint8_t Pixel::getGreen() { return this->Green; }

uint8_t Pixel::getBlue() { return this->Blue; }

void Pixel::setRed(const uint8_t &Red) {
  if (Red < 32) {
    this->Red = Red;
  } else {
    this->Red = 0;
  }
}

void Pixel::setGreen(const uint8_t &Green) {
  if (Green < 64) {
    this->Green = Green;
  } else {
    this->Green = 0;
  }
}

void Pixel::setBlue(const uint8_t &Blue) {
  if (Blue < 32) {
    this->Blue = Blue;
  } else {
    this->Blue = 0;
  }
}
