/// @file
/// @brief Bunker module source file

#include <iostream>

#include "Bullet.h"
#include "Bunker.h"
#include "Sprite.h"

Bunker::Bunker(Sprite InitSprite, const int &InitY, const int &InitX)
    : Entity(InitSprite, InitY, InitX) {}
bool Bunker::hit(Bullet &InBullet) {
  bool Ret = false;
  if (InBullet.getDirection() == Up) {
    if ((InBullet.getY() > getY()) &&
        (InBullet.getY() < (getY() + getSprite().getHeight())) &&
        (InBullet.getX() > getX()) &&
        (InBullet.getX() < (getX() + getSprite().getWidth()))) {
      Ret = getSprite().damage(InBullet.getY() - getY(),
                               InBullet.getX() - getX(), 2, DamageUp);
    }
  } else if (InBullet.getDirection() == Down) {
    int ActualBulletY = InBullet.getY() - InBullet.getSprite().getHeight() + 1;
    if ((ActualBulletY > getY()) &&
        (ActualBulletY < (getY() + getSprite().getHeight())) &&
        (InBullet.getX() > getX()) &&
        (InBullet.getX() < (getX() + getSprite().getWidth()))) {
      Ret = getSprite().damage(ActualBulletY - getY(), InBullet.getX() - getX(),
                               3, DamageDown);
    }
  }
  return Ret;
}
