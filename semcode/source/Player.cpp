/// @file
/// @brief Player module source file

#include <iostream>

#include "Bullet.h"
#include "Entity.h"
#include "Player.h"
#include "Sprite.h"

Player::Player(Sprite InExplosionSprite, Sprite PlayerSprite,
               const std::size_t &InitY, const std::size_t &InitX,
               int InitHealth)
    : Entity(PlayerSprite, InitY, InitX), ExplosionSprite(InExplosionSprite) {
  Health = InitHealth;
  Exploded = false;
  Shot = false;
  Score = 0;
}

bool Player::hit(Bullet &InBullet) {
  int ActualBulletY = InBullet.getY() + InBullet.getSprite().getHeight() - 1;
  if ((!isHit() && (ActualBulletY > getY()) &&
       ActualBulletY < (getY() + getSprite().getHeight())) &&
      (InBullet.getX() > getX()) &&
      (InBullet.getX() < (getX() + getSprite().getWidth()))) {
    Health--;
    Exploded = true;
    return true;
  }
  return false;
}

Sprite Player::getExplosionSprite() {
  Exploded = false;
  return ExplosionSprite;
}

bool Player::isHit() { return Exploded; }

int Player::getHealth() { return Health; }

void Player::setHealth(const int &InHealth) { Health = InHealth; };
void Player::setScore(const int &InScore) { Score = InScore; }
void Player::incrementScore(unsigned int Increment) { Score += Increment; }

unsigned int Player::getScore() { return Score; }
