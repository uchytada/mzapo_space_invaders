/// @file
/// @brief Menu module source file

#include <string>

#include "Display.h"
#include "Menu.h"
#include "Text.h"

Menu::Menu(Grid &InPixelGrid) : PixelGrid(InPixelGrid) {
  Text Options(Style, PixelGrid);
  Text Title(Style, PixelGrid);
  Title.drawLine(TitlePosY, TitlePosX, StrTitle, TitleColor[0], TitleColor[1],
                 TitleColor[2]);
  for (int i = 0; i < CountOpt; i++) {
    Options.drawLine(OptPosY[i], OptPosX[i], Option[i], DefColor[0],
                     DefColor[1], DefColor[2]);
  }
}

unsigned int Menu::setOptions(int8_t Increment, bool Button) {
  Text Title(Style, PixelGrid);
  Title.drawLine(TitlePosY, TitlePosX, StrTitle, TitleColor[0], TitleColor[1],
                 TitleColor[2]);

  if (Increment > 0 && Cursor < CountOpt - 1) {
    ++Cursor;
  } else if (Increment < 0 && Cursor > 0) {
    --Cursor;
  }

  Text Options(Style, PixelGrid);
  for (int i = 0; i < CountOpt; i++) {
    Options.drawLine(OptPosY[i], OptPosX[i], Option[i], DefColor[0],
                     DefColor[1], DefColor[2]);
  };

  Options.drawLine(OptPosY[Cursor], OptPosX[Cursor], Option[Cursor],
                   Highlight[0], Highlight[1], Highlight[2]);

  if (Button) {
    return Cursor + 1;
  }
  return 0;
}

unsigned int Menu::setOptionsKeyboard(const int &KeyboardRet) {
  if (KeyboardRet == 100) {
    return setOptions(0, false);
  } else if (KeyboardRet == 0) {
    return setOptions(0, true);
  } else {
    return setOptions(KeyboardRet, false);
  }
}
