/// @file
/// @brief Knobs module source file

#include <cmath>
#include <iostream>
#include <stdexcept>

#include "Knobs.h"
#include "TimeStamp.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

Knob::Knob(uint8_t InNumberKnob) {
  NumberKnob = InNumberKnob;
  Last = 0;
  IgnoreTime = 0;
  IgnoreButton = false;
  try {
    MemBase = static_cast<uint8_t *>(
        map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0));
    if (MemBase == NULL) {
      throw std::runtime_error("Failed to map physical address");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }
}

int8_t Knob::increment() {
  const uint8_t IgnorVal = 100;
  uint8_t KnobByte =
      *(volatile uint8_t *)(MemBase + SPILED_REG_KNOBS_8BIT_o + NumberKnob);
  int8_t Increment = KnobByte - Last;
  Last = KnobByte;
  if (abs(Increment) > IgnorVal) {
    return 0;
  } else {
    return Increment;
  }
}

bool Knob::button() {
  if (!IgnoreButton || currentTimestamp() > IgnoreTime) {
    uint8_t ButtonByte =
        *(volatile uint8_t *)(MemBase + SPILED_REG_KNOBS_8BIT_o + 0x3);
    uint8_t ButtonBit = (ButtonByte >> NumberKnob) & 0x1;

    IgnoreButton = false;
    if (ButtonBit == true) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

void Knob::ignoreButtonMS(unsigned int InMS) {
  IgnoreButton = true;
  IgnoreTime = currentTimestamp() + InMS;
}
