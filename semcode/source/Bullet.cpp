/// @file
/// @brief Bullet module source file

#include "Bullet.h"
#include "Entity.h"
#include "Sprite.h"

Bullet::Bullet(Sprite InitSprite, const int &InitY, const int &InitX,
               const Origin &InOrigin, const Direction &InDirection)
    : Entity(InitSprite, InitY, InitX) {
  BulletDirection = InDirection;
  BulletOrigin = InOrigin;
}

Direction Bullet::getDirection() { return BulletDirection; }
Origin Bullet::getOrigin() { return BulletOrigin; }
