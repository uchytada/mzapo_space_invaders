/// @file
/// @brief Sprite module source file

#include <array>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "Display.h"
#include "Sprite.h"

Sprite::Sprite(const std::array<uint32_t, 16> &Asset, const int AssetWidth) {

  try {
    if (Asset.empty()) {
      throw std::runtime_error("Received empty asset");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }

  for (auto it = Asset.cbegin(); it != Asset.cend(); ++it) {
    std::vector<Pixel> Line;
    for (int i = 1; i <= AssetWidth; ++i) {
      if ((*(it) >> (AssetWidth - i)) & 0x1) {
        Line.push_back(Pixel(31, 31, 31));
      } else {
        Line.push_back(Pixel(0, 0, 0));
      }
    }
    Pixels.push_back(Line);
  }
  SpriteWidth = Pixels[0].size();
  SpriteHeight = Pixels.size();
}

Sprite::Sprite(const std::array<uint8_t, 8> &Asset, const int AssetWidth) {

  try {
    if (Asset.empty()) {
      throw std::runtime_error("Received empty asset");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }

  for (auto it = Asset.cbegin(); it != Asset.cend(); ++it) {
    std::vector<Pixel> Line;
    for (int i = 1; i <= AssetWidth; ++i) {
      if ((*(it) >> (AssetWidth - i)) & 0x1) {
        Line.push_back(Pixel(31, 31, 31));
      } else {
        Line.push_back(Pixel(0, 0, 0));
      }
    }
    Pixels.push_back(Line);
  }
  SpriteWidth = Pixels[0].size();
  SpriteHeight = Pixels.size();
}

Sprite::Sprite(const std::array<uint64_t, 32> &Asset, const int AssetWidth) {

  try {
    if (Asset.empty()) {
      throw std::runtime_error("Received empty asset");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }

  for (auto it = Asset.cbegin(); it != Asset.cend(); ++it) {
    std::vector<Pixel> Line;
    for (int i = 1; i <= AssetWidth; ++i) {
      if ((*(it) >> (AssetWidth - i)) & 0x1) {
        Line.push_back(Pixel(31, 31, 31));
      } else {
        Line.push_back(Pixel(0, 0, 0));
      }
    }
    Pixels.push_back(Line);
  }
  SpriteWidth = Pixels[0].size();
  SpriteHeight = Pixels.size();
}

bool Sprite::damage(int Y, int X, const int &Radius,
                    const DamageDirection &InDamageDirection) {
  bool Ret = false;
  if (Pixels[Y][X].getRed() != 0 || Pixels[Y][X].getGreen() != 0 ||
      Pixels[Y][X].getBlue() != 0) {
    if (InDamageDirection == DamageUp) {
      for (int i = SpriteHeight - 1; i >= Y - 2 && i >= 0; --i) {
        for (int j = 0; j <= Radius; ++j) {
          if ((X - j) >= 0) {
            Pixels[i][X - j].setPixel(0, 0, 0);
          }
          Pixels[i][X].setPixel(0, 0, 0);
          if ((X + j) < SpriteWidth) {
            Pixels[i][X + j].setPixel(0, 0, 0);
          }
        }
      }

      Ret = true;
    } else if (InDamageDirection == DamageDown) {
      for (int i = Y; i >= 0; --i) {
        for (int j = 0; j <= Radius; ++j) {
          if ((X - j) >= 0) {
            Pixels[i][X - j].setPixel(0, 0, 0);
          }
          Pixels[i][X].setPixel(0, 0, 0);
          if ((X + j) < SpriteWidth) {
            Pixels[i][X + j].setPixel(0, 0, 0);
          }
        }
      }
      Ret = true;
    }
  }
  return Ret;
}

Pixel Sprite::getPixel(const int &Y, const int &X) {
  if (X < SpriteWidth && Y < SpriteHeight) {
    return Pixels[Y][X];
  } else {
    return Pixel(0, 0, 0);
  }
}

Pixel Sprite::getPixel(const int &Y, const int &X) const {
  return const_cast<Sprite *>(this)->getPixel(Y, X);
}

void Sprite::setColor(const int &Red, const int &Green, const int &Blue) {
  for (auto &Column : Pixels) {
    for (auto &Element : Column) {
      if (Element.getRed() != 0 && Element.getGreen() != 0 &&
          Element.getBlue() != 0) {
        Element.setPixel(Red, Green, Blue);
      }
    }
  }
}

int Sprite::getWidth() { return SpriteWidth; }
int Sprite::getWidth() const { return SpriteWidth; }

int Sprite::getHeight() const { return SpriteHeight; }
int Sprite::getHeight() { return SpriteHeight; }

void drawSprite(Grid &Pixels, int SpriteY, int SpriteX,
                const Sprite &InSprite) {
  for (int y = 0; y < InSprite.getHeight(); ++y) {
    for (int x = 0; x < InSprite.getWidth(); ++x) {
      Pixels[y + SpriteY][x + SpriteX] = InSprite.getPixel(y, x);
    }
  }
}
