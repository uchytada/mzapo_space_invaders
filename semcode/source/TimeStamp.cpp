/// @file
/// @brief Timestamp module source file

#include <cstddef>
#include <sys/time.h>

#include "TimeStamp.h"

long long currentTimestamp() {
  struct timeval TimeVal;
  gettimeofday(&TimeVal, NULL); // get current time
  long long MS = TimeVal.tv_sec * 1000LL +
                 TimeVal.tv_usec / 1000LL; // calculate milliseconds
  return MS;
}
