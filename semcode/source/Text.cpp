/// @file
/// @brief Text module source file

#include <array>
#include <cstdint>

#include "Display.h"
#include "Text.h"
#include "font_types.h"

Text::Text(const uint8_t &Style, Grid &InPixels) : GridPixels(InPixels) {
  switch (Style) {
  case 1:
    FontDes = &font_winFreeSystem14x16;
    break;
  case 2:
    FontDes = &font_rom8x16;
    break;
  default:
    FontDes = &font_winFreeSystem14x16;
    break;
  }
}

void Text::drawLine(const uint32_t &Y, const uint32_t &X,
                    const std::string &Line, const uint8_t &InRed,
                    const uint8_t &InGreen, const uint8_t &InBlue) {
  Red = InRed;
  Green = InGreen;
  Blue = InBlue;
  uint32_t ActualX = X;
  uint32_t ActualY = Y;
  for (unsigned int i = 0; i < Line.length(); i++) {
    drawChar(ActualY, ActualX, Line[i]);
    ActualX += FontDes->width[Line[i] - FontDes->firstchar];
  }
}

void Text::drawChar(const uint32_t &Y, const uint32_t &X, const char &Chr) {
  unsigned int Offset =
      (static_cast<unsigned int>(Chr) - FontDes->firstchar) * FontDes->height;
  for (unsigned int h = Offset; h < (Offset + FontDes->height); ++h) {
    uint16_t Line = FontDes->bits[h];
    for (int w = 0; w < FontDes->maxwidth; ++w) {
      uint8_t Bit = (Line >> (15 - w) & 0x1);
      GridPixels[h - Offset + Y][w + X] =
          Pixel(Red * Bit, Green * Bit, Blue * Bit);
    }
  }
}
