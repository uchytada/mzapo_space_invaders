/// @file
/// @brief Game module source file
#include <cstdlib>
#include <ctime>
#include <vector>

#include "Bullet.h"
#include "Bunker.h"
#include "Enemy.h"
#include "Game.h"
#include "Keyboard.h"
#include "Knobs.h"
#include "TimeStamp.h"

#ifndef WIDTH
#define WIDTH 480
#endif

#ifndef HEIGHT
#define HEIGHT 320
#endif

#define MAX_ENEMY_HEIGHT 16
#define MAX_ENEMY_WIDTH 32
#define ENEMY_HEIGHT_OFFSET 5
#define ENEMIES_Y_OFFSET 40
#define PLAYER_BULLET_X_OFFSET 14
#define BULLET_STEP 5
#define COOLDOWN_PERIOD 500
#define INCREMENT_COEFICIENT 5
#define PLAYER_X_BOUNDARY 448

bool enemyHit(std::vector<Enemy> &InEnemies,
              std::vector<Bullet>::iterator BulletIter, Player &InPlayer) {
  bool Hit = false;
  for (auto EnemyIter = InEnemies.begin(); EnemyIter != InEnemies.end();) {
    if (BulletIter->getY() >= EnemyIter->getY() &&
        (BulletIter->getY() <=
         (EnemyIter->getY() + EnemyIter->getSprite().getHeight())) &&
        (BulletIter->getX() >= (EnemyIter->getX())) &&
        (BulletIter->getX() <=
         (EnemyIter->getX() +
          EnemyIter->getSprite().getWidth()))) { // Basically bullet is inside
                                                 // enemy hitbox (Sprite)
      EnemyIter->setExploded();
      InPlayer.incrementScore(EnemyIter->getScoreGain());
      EnemyIter = InEnemies.end();
      Hit = true;
    } else {
      ++EnemyIter;
    }
  }
  return Hit;
}

bool bunkerHit(std::vector<Bunker> &InBunkers,
               std::vector<Bullet>::iterator BulletIter) {
  bool Hit = false;
  for (auto BunkerIter = InBunkers.begin(); BunkerIter != InBunkers.end();
       ++BunkerIter) {
    Hit = BunkerIter->hit(*(BulletIter));
    if (Hit) {
      break;
    }
  }
  return Hit;
}

bool playerHit(Player &InPlayer, std::vector<Bullet>::iterator BulletIter) {
  return InPlayer.hit(*BulletIter);
}

void clearPixelGrid(Grid &Pixels) {
  for (int i = 0; i < HEIGHT; ++i) {
    for (int j = 0; j < WIDTH; ++j) {
      Pixels[i][j].setPixel(0, 0, 0);
    }
  }
}

void drawBunkers(Grid &Pixels, const std::vector<Bunker> &Bunkers) {
  for (auto it = Bunkers.cbegin(); it != Bunkers.cend(); ++it) {
    drawSprite(Pixels, it->getY(), it->getX(), it->getSprite());
  }
}

void initEnemies(std::vector<Enemy> &InEnemies) {
  while (!InEnemies.empty()) {
    InEnemies.pop_back();
  }
  for (int i = 2; i < 13; ++i) { // The x starting position of the alien is
                                 // determined by expression i*32. i runs from
                                 // 2 to 12 to properly center the aliens.
    // Row 1 - Alien type C
    InEnemies.push_back(
        Enemy(ALIENC_SCORE, Sprite(AlienExplodingSprite, ALIEN_EXPLODING_WIDTH),
              Sprite(AlienCMoving, ALIENC_WIDTH), Sprite(AlienC, ALIENC_WIDTH),
              i - 2, ENEMIES_Y_OFFSET,
              i * MAX_ENEMY_WIDTH +
                  3)); // This alien is smaller than the others, so there
                       // needs to +3 offset to make it more centered.
    // Row 2 - Alien type B
    InEnemies.push_back(Enemy(
        ALIENB_SCORE, Sprite(AlienExplodingSprite, ALIEN_EXPLODING_WIDTH),
        Sprite(AlienBMoving, ALIENB_WIDTH), Sprite(AlienB, ALIENB_WIDTH), i - 2,
        ENEMIES_Y_OFFSET + (MAX_ENEMY_HEIGHT + ENEMY_HEIGHT_OFFSET),
        i * MAX_ENEMY_WIDTH));

    // Row 3 - Alien type B
    InEnemies.push_back(Enemy(
        ALIENB_SCORE, Sprite(AlienExplodingSprite, ALIEN_EXPLODING_WIDTH),
        Sprite(AlienBMoving, ALIENB_WIDTH), Sprite(AlienB, ALIENB_WIDTH), i - 2,
        ENEMIES_Y_OFFSET + 2 * (MAX_ENEMY_HEIGHT + ENEMY_HEIGHT_OFFSET),
        i * MAX_ENEMY_WIDTH));

    // Row 4 - Alien type A
    InEnemies.push_back(Enemy(
        ALIENA_SCORE, Sprite(AlienExplodingSprite, ALIEN_EXPLODING_WIDTH),
        Sprite(AlienAMoving, ALIENA_WIDTH), Sprite(AlienA, ALIENA_WIDTH), i - 2,
        ENEMIES_Y_OFFSET + 3 * (MAX_ENEMY_HEIGHT + ENEMY_HEIGHT_OFFSET),
        i * MAX_ENEMY_WIDTH));

    // Row 5 - Alien type A
    InEnemies.push_back(Enemy(
        ALIENA_SCORE, Sprite(AlienExplodingSprite, ALIEN_EXPLODING_WIDTH),
        Sprite(AlienAMoving, ALIENA_WIDTH), Sprite(AlienA, ALIENA_WIDTH), i - 2,
        ENEMIES_Y_OFFSET + 4 * (MAX_ENEMY_HEIGHT + ENEMY_HEIGHT_OFFSET),
        i * MAX_ENEMY_WIDTH));
  }
}

void resolveBullets(std::vector<Bullet> &InBullets,
                    std::vector<Enemy> &InEnemies, std::vector<Bunker> &Bunkers,
                    Player &InPlayer, Grid &InPixels) {
  for (auto BulletIter = InBullets.begin();
       BulletIter != InBullets.end();) { // Iterating trough all the bullets
    switch (BulletIter->getOrigin()) {
    case PlayerOrigin: {
      if (bunkerHit(Bunkers, BulletIter) ||
          enemyHit(InEnemies, BulletIter, InPlayer)) { // Bullet hit something
        BulletIter = InBullets.erase(BulletIter);      // erase it
      } else {
        drawSprite(InPixels, BulletIter->getY(), BulletIter->getX(),
                   BulletIter->getSprite());
        int newY = BulletIter->getY() - BULLET_STEP;
        if (newY < 0) {
          BulletIter = InBullets.erase(BulletIter);
        } else {
          BulletIter->setY(newY);
          ++BulletIter;
        }
      }
    } break;
    case EnemyOrigin: {
      if (bunkerHit(Bunkers, BulletIter) || playerHit(InPlayer, BulletIter)) {
        BulletIter = InBullets.erase(BulletIter);
      } else {
        drawSprite(InPixels, BulletIter->getY(), BulletIter->getX(),
                   BulletIter->getSprite());
        int newY = BulletIter->getY() + BULLET_STEP;
        int ActualBulletY = newY + BulletIter->getSprite().getHeight() - 1;
        if (ActualBulletY >= HEIGHT - 5) { // Bullet reached end of the screen
          BulletIter = InBullets.erase(BulletIter);
        } else {
          BulletIter->setY(newY);
          ++BulletIter;
        }
      }
    } break;
    default:
      break;
    }
  }
}

void resolveEnemies(std::vector<Enemy> &InEnemies,
                    std::vector<Bullet> &InBullets, Grid &InPixels,
                    bool &InGameOver, const int &InShootFrequency) {
  int EnemyCount = InEnemies.size();
  static long long DescentTime = 0;
  bool Descented = false;
  for (auto it = InEnemies.begin(); it != InEnemies.end();) {
    if (it->exploaded()) {
      drawSprite(InPixels, it->getY(), it->getX(), it->getExplosionSprite());
      InEnemies.erase(it);
    } else {
      drawSprite(InPixels, it->getY(), it->getX(), it->getMovingSprite());
      if (((it + 1) == InEnemies.end() ||
           ((it + 1)->getColumn() != it->getColumn())) &&
          (((rand() % InShootFrequency) == 0))) {
        InBullets.push_back(Bullet(Sprite(PlayerShotSprite, PLAYER_SHOT_WIDTH),
                                   it->getY(), it->getX() + 10, EnemyOrigin,
                                   Down));
      }
      switch (it->getState()) { // This switch case handles move of enemies from
                                // right to left.
      case MovingLeft: {
        int newX = it->getX() - 3;
        it->setX(newX);
      } break;
      case MovingRight: {
        int newX = it->getX() + 3;
        it->setX(newX);
      } break;
      default:
        break;
      }
      if (EnemyCount < 55 && ((currentTimestamp() - DescentTime) > 1000)) {
        int newY = it->getY();
        switch (EnemyCount) { // This switch case determines the speed at which
                              // will the enemies descend on player. It is based
                              // on amount of enemies alive. Less enemies alive
                              // -> faster descend.
        case 45 ... 53:
          newY += 1;
          break;
        case 24 ... 44:
          newY += 2;
          break;
        case 15 ... 23:
          newY += 4;
          break;
        case 8 ... 14:
          newY += 5;
          break;
        case 2 ... 7:
          newY += 6;
          break;
        case 1:
          newY += 9;
          break;
        default:
          break;
        }
        Descented = true;

        if (newY >= HEIGHT - 20) { // Enemy reached earth -> Game Over
          InGameOver = true;
        } else {
          it->setY(newY);
        }
      }
      ++it;
    }
  }
  if (Descented) {
    Descented = false;
    DescentTime = currentTimestamp();
  }
}

void resolvePlayer(Player &InPlayer, std::vector<Bullet> &InBullets,
                   Knob &InKnob, Grid &InPixels, const bool &InKeyboard) {
  static long long PrevTime = currentTimestamp();

  if (!InKeyboard) {
    if (InKnob.button() &&
        ((currentTimestamp() - PrevTime) >= COOLDOWN_PERIOD)) {
      InBullets.push_back(
          Bullet(Sprite(PlayerShotSprite, PLAYER_SHOT_WIDTH), InPlayer.getY(),
                 InPlayer.getX() + PLAYER_BULLET_X_OFFSET, PlayerOrigin, Up));
      PrevTime = currentTimestamp();
    }
  } else {
    if ((keyboard() == 0) &&
        ((currentTimestamp() - PrevTime) >= COOLDOWN_PERIOD)) {
      InBullets.push_back(
          Bullet(Sprite(PlayerShotSprite, PLAYER_SHOT_WIDTH), InPlayer.getY(),
                 InPlayer.getX() + PLAYER_BULLET_X_OFFSET, PlayerOrigin, Up));
      PrevTime = currentTimestamp();
    }
  }

  if (InPlayer.isHit()) {
    drawSprite(InPixels, InPlayer.getY(), InPlayer.getX(),
               InPlayer.getExplosionSprite());
  } else {
    drawSprite(InPixels, InPlayer.getY(), InPlayer.getX(),
               InPlayer.getSprite());
  }
  int newX;
  if (!InKeyboard) {
    newX = InPlayer.getX() - INCREMENT_COEFICIENT * InKnob.increment();
  } else {
    int Increment = keyboard();
    newX = InPlayer.getX();
    if (Increment != 100) {
      newX -= INCREMENT_COEFICIENT * Increment;
    }
  }

  if (newX > PLAYER_X_BOUNDARY) {
    InPlayer.setX(PLAYER_X_BOUNDARY - 1);
  } else if (newX < 0) {
    InPlayer.setX(0);
  } else {
    InPlayer.setX(newX);
  }
}
