/// @file
/// @brief Keyboard module source file

#include <cstdio>
#include <fcntl.h>
#include <iostream>
#include <poll.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

#include "Keyboard.h"

#define TIMEOUT 25

int keyboard() {
  int ret = 100;
  unsigned char c;

  if (io_getc_timeout(STDIN_FILENO, TIMEOUT, &c) > 0) {
    switch (c) {
    case 'a':
      ret = 1;
      break;
    case 'd':
      ret = -1;
      break;
    case ' ':
      ret = 0;
    default:
      break;
    }
  } else {
    ret = 100;
  }

  return ret;
}

// Taken from B3B36PRG Semestral task
void call_termios(int reset) {
  static struct termios tio, tioOld;
  tcgetattr(STDIN_FILENO, &tio);
  if (reset) {
    tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
  } else {
    tioOld = tio;
    cfmakeraw(&tio);
    tio.c_oflag |= OPOST;
    tcsetattr(STDIN_FILENO, TCSANOW, &tio);
  }
}

// Taken from B3B36PRG Semestral task
int io_getc_timeout(int fd, int timeout_ms, unsigned char *c) {
  struct pollfd ufdr[1];
  int r = 0;
  ufdr[0].fd = fd;
  ufdr[0].events = POLLIN | POLLRDNORM;
  if ((poll(&ufdr[0], 1, timeout_ms) > 0) &&
      (ufdr[0].revents & (POLLIN | POLLRDNORM))) {
    r = read(fd, c, 1);
  }
  return r;
}
