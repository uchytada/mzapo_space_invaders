/// @file
/// @brief Entity module source file

#include "Entity.h"
#include "Sprite.h"

Entity::Entity(Sprite InitSprite, const int &InitY, const int &InitX)
    : EntitySprite(InitSprite) {
  X = InitX;
  Y = InitY;
}

Sprite &Entity::getSprite() { return EntitySprite; }

Sprite Entity::getSprite() const { return EntitySprite; }

int Entity::getX() { return X; }

int Entity::getY() { return Y; }

int Entity::getX() const { return X; }

int Entity::getY() const { return Y; }

void Entity::setY(const int &NewY) { Y = NewY; }

void Entity::setX(const int &NewX) { X = NewX; }
