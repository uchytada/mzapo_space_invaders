/// @file
/// @brief LEDLine module source file

#include <iostream>
#include <stdexcept>

#include "LEDLine.h"
#include "TimeStamp.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

LEDLine::LEDLine(const uint32_t &InPeriod) {
  t1 = 0;
  LEDBits = 7;
  ActualLED = 0;
  Period = InPeriod;
  FlagLED = false;
  try {
    MemBase = static_cast<uint8_t *>(
        map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0));
    if (MemBase == NULL) {
      throw std::runtime_error("Failed to map physical address");
    }
  } catch (std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }
}

void LEDLine::ledShift() {
  double MSDiff = (currentTimestamp() - t1);
  if (MSDiff >= Period && ActualLED < 30) {
    *(volatile uint32_t *)(MemBase + SPILED_REG_LED_LINE_o) = LEDBits;
    LEDBits <<= 1;
    t1 = currentTimestamp();
    ++ActualLED;
  } else if (ActualLED >= 30) {
    LEDBits = 7;
    ActualLED = 0;
  }
}
