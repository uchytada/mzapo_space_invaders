/// @file
/// @brief main file

/******************************************************************* Space
 Invaders project based on main function template for MicroZed based MZ_APO
 board designed by Petr Porazil at PiKRON

  main.cpp      - main file
  Game.h/cpp    - Game function module
  Assets.h/cpp  - Assets file
  Enemy.h/cpp   - Enemy class
  Player.h/cpp  - Player class
  TimeStamp.h/cpp - currentTimestamp function
  Bullet.h/cpp  - Bullet class
  Entity.h/cpp  - Entity class
  Keyboard.h/cpp - keyboard module
  Menu.h/cpp    - Menu class
  RGBLEDs.h/cpp - RGBLED class
  Bunker.h/cpp  - Bunker class
  Knobs.h/cpp   - Knob class
  Sprite.h/cpp  - Sprite class
  Display.h/cpp - Display + Pixel class
  LEDLine.h/cpp - LEDLine class
  Text.h/cpp    - Text class

  MIT License

  Copyright (c) 2020 Adam Uchytil, Jakub Diezka

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <array>
#include <iostream>
#include <string>
#include <vector>

#include "Assets.h"
#include "Bullet.h"
#include "Bunker.h"
#include "Display.h"
#include "Enemy.h"
#include "Entity.h"
#include "Game.h"
#include "Keyboard.h"
#include "Knobs.h"
#include "LEDLine.h"
#include "Menu.h"
#include "Player.h"
#include "RGBLEDs.h"
#include "Sprite.h"
#include "Text.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#ifndef WIDTH
#define WIDTH 480
#endif

#ifndef HEIGHT
#define HEIGHT 320
#endif

#define BUNKER_Y_OFFSET 64
#define PLAYER_HEALTH 3
#define PLAYER_DEFAULT_X 224
#define PLAYER_DEFAULT_Y 300
#define BUTTON_IGNORE_MS 1000

int main(int argc, char *argv[]) {
  std::vector<Enemy> Enemies;
  std::vector<Bullet> Bullets;
  std::vector<Bunker> Bunkers;
  bool Keyboard = false;
  int KnobNumber = 0;

  int Difficulty = 1;
  const int DifficultyField[3] = {253, 113, 57};

  if (argc == 2) {
    std::string Arg(argv[1]);
    if (Arg == "--keyboard") {
      Keyboard = true;
    } else if (Arg == "--encoder1") {
      KnobNumber = 2;
    } else if (Arg == "--encoder2") {
      KnobNumber = 1;
    } else if (Arg == "--encoder3") {
      KnobNumber = 0;
    } else {
      std::cerr << "Invalid command line argument!" << std::endl;
      return EXIT_FAILURE;
    }
  } else if (argc >= 3) {
    std::cerr << "Too many command line arguments!" << std::endl;
    return EXIT_FAILURE;
  }

  if (Keyboard) {
    call_termios(0);
  }

  Display Display0;
  Grid Pixels;
  Knob ControlKnob(KnobNumber);
  LEDLine Line(50);
  RGBLED LED0(0, 255, 0, 0);

  clearPixelGrid(Pixels);
  Text Score(1, Pixels);
  Menu GameMenu(Pixels);
  clearPixelGrid(Pixels);
  GameState ActualState = StateMenu;

  Player Player0(Sprite(PlayerBlowupSprite1, PLAYER_WIDTH),
                 Sprite(PlayerSprite, PLAYER_WIDTH), PLAYER_DEFAULT_Y,
                 PLAYER_DEFAULT_X, 3);
  Player0.getSprite().setColor(0, 31, 0);

  bool GameOver = false;
  bool quit = false;

  int Period = 1000;
  while (!quit) {
    switch (ActualState) {
    case StateMenu: {
      if (!Keyboard) {
        while ((Difficulty = GameMenu.setOptions(ControlKnob.increment(),
                                                 ControlKnob.button())) == 0) {
          Display0.setFrameBuffer(Pixels);
          Display0.redraw();
          clearPixelGrid(Pixels);
        }
      } else {
        while ((Difficulty = GameMenu.setOptionsKeyboard(keyboard())) == 0) {
          Display0.setFrameBuffer(Pixels);
          Display0.redraw();
          clearPixelGrid(Pixels);
        }
      }
      Difficulty -= 1;
      if (Difficulty == 3) { // Menu == EXIT
        if (Keyboard) {
          call_termios(1);
        }
        return EXIT_SUCCESS;
      }
      ActualState = StateInit;
    } break;
    case StateInit: {

      while (!Bullets.empty()) { // Clear all (previous) bulllets
        Bullets.pop_back();
      }
      while (!Bunkers.empty()) { // Clear all (previous) bunkers
        Bunkers.pop_back();
      }

      for (int i = 0; i != 4; ++i) { // Init bunkres

        Sprite BunkerSprite(ShieldImage, SHIELD_WIDTH);
        BunkerSprite.setColor(0, 31, 0);
        Bunkers.push_back(
            Bunker(BunkerSprite, HEIGHT - BUNKER_Y_OFFSET, 56 + i * (32 + 80)));
      }

      initEnemies(Enemies);

      // reinitalize player
      Player0.setScore(0);
      Player0.setHealth(PLAYER_HEALTH);
      Player0.setX(PLAYER_DEFAULT_X);
      Player0.setY(PLAYER_DEFAULT_Y);

      ActualState = StateGameLoop;
    } break;
    case StateGameLoop: {
      clearPixelGrid(Pixels);
      Score.drawLine(0, 0, "Score: " + std::to_string(Player0.getScore()), 31,
                     31, 31); // Draw score

      drawBunkers(Pixels, Bunkers);
      resolveBullets(Bullets, Enemies, Bunkers, Player0, Pixels);
      resolveEnemies(Enemies, Bullets, Pixels, GameOver,
                     DifficultyField[Difficulty]);

      if (Enemies.size() == 0) { // All enemies are dead -> new wave
        initEnemies(Enemies);
      }
      resolvePlayer(Player0, Bullets, ControlKnob, Pixels, Keyboard);

      switch (
          Player0.getHealth()) { // LED period and color based on player health
      case 3:
        LED0.setColor(0, 255, 0);
        Period = 1000;
        break;
      case 2:
        LED0.setColor(255, 52, 0);
        Period = 200;
        break;
      case 1:
        LED0.setColor(255, 0, 0);
        Period = 50;
        break;
      case 0:
        GameOver = true;
        break;
      default:
        break;
      }

      Display0.setFrameBuffer(Pixels);
      Display0.redraw(); // Frame buffer -> Display
      Line.ledShift();   // LED line (snake)
      LED0.blink(Period);

      if (GameOver) {
        ActualState = StateGameOver;
      }
    } break;
    case StateGameOver: {
      Text GameOverText(1, Pixels);
      if (!Keyboard) {
        ControlKnob.ignoreButtonMS(BUTTON_IGNORE_MS);
        while (!ControlKnob.button()) {
          GameOverText.drawLine(0, 200, std::string("GAME OVER"), 31, 0, 0);
          Display0.setFrameBuffer(Pixels);
          Display0.redraw();
        }
        ControlKnob.ignoreButtonMS(
            BUTTON_IGNORE_MS); // ignore input for some time so player doesnt
                               // accidentaly start new game
      } else {
        while (keyboard() == 100) {
          GameOverText.drawLine(0, 200, std::string("GAME OVER"), 31, 0, 0);
          Display0.setFrameBuffer(Pixels);
          Display0.redraw();
        }
      }
      GameOver = false;
      ActualState = StateMenu;
    } break;
    }
  }
  if (Keyboard) {
    call_termios(1);
  }
  return 0;
}
