/// @file
/// @brief RGBLEDs module source file

#include "RGBLEDs.h"
#include "TimeStamp.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include <cstdint>
#include <ctime>
#include <iostream>
#include <stdexcept>
#include <unistd.h>
#include <vector>

RGBLED::RGBLED(const uint8_t &InLedNum, const uint8_t &InRed,
               const uint8_t &InGreen, const uint8_t &InBlue) {
  setColor(InRed, InGreen, InBlue);
  t1 = 0;
  ActualCycle = 0;
  FlagLed = true;

  try {
    MemBase = static_cast<uint8_t *>(
        map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0));
    MemLed = MemBase;
    if (MemBase == NULL) {
      throw std::runtime_error("Failed to map physical address");
    }
  } catch (const std::runtime_error &Exception) {
    std::cerr << Exception.what() << std::endl;
    exit(1);
  }

  if (InLedNum == 0) {
    MemLed += SPILED_REG_LED_RGB1_o;
  } else {
    MemLed += SPILED_REG_LED_RGB2_o;
  }
}

void RGBLED::setColor(const uint8_t &InRed, const uint8_t &InGreen,
                      const uint8_t &InBlue) {
  Red = InRed;
  Green = InGreen;
  Blue = InBlue;
}
void RGBLED::flip() {
  uint8_t ValRed;
  uint8_t ValGreen;
  uint8_t ValBlue;
  if (FlagLed) {
    ValRed = Red;
    ValGreen = Green;
    ValBlue = Blue;
  } else {
    ValRed = 0;
    ValGreen = 0;
    ValBlue = 0;
  }
  uint32_t RGBVal = ValBlue | (ValRed << 16) | (ValGreen << 8);
  *(volatile uint32_t *)(MemLed) = RGBVal;
  FlagLed = !FlagLed;
}

void RGBLED::blink(const uint32_t &Period) {
  float MSDiff = currentTimestamp() - t1;
  if (MSDiff >= Period) {
    RGBLED::flip();
    t1 = currentTimestamp();
  }
}
