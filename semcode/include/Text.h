/// @file
/// @brief Text module header file

#include <string>

#include "Display.h"
#include "font_types.h"

#ifndef TEXT_H
#define TEXT_H

class Text {
public:
  /// @brief  Text object constructor
  ///
  /// @param Style  Value of 1 or 2 representing font style
  ///               1 : FreeSystem14x16
  //                2 : rom8x16
  /// @param InPixels Reference grid of Pixel (text destination)
  Text(const uint8_t &Style, Grid &InPixels);

  /// @brief  Draws line of text into "InPixels" given at construction
  ///
  /// @param Y  Starting Y coordinate
  /// @param X  Starting X coordinate
  /// @param Line  std::string object containing the actual text
  /// @param InRed  Red component of the color of the text (range 0 - 31)
  /// @param InGreen  Green component of the color of the text (range 0 - 31)
  /// @param InBlue  Blue component of the color of the text (range 0 - 31)
  void drawLine(const uint32_t &Y, const uint32_t &X, const std::string &Line,
                const uint8_t &InRed, const uint8_t &InGreen,
                const uint8_t &InBlue);

private:
  uint8_t Red;
  uint8_t Green;
  uint8_t Blue;
  Grid &GridPixels;
  font_descriptor_t *FontDes;
  void drawChar(const uint32_t &Y, const uint32_t &X, const char &Chr);
};

#endif
