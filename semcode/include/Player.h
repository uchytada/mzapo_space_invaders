/// @file
/// @brief Player module header file

#include "Bullet.h"
#include "Entity.h"
#include "Sprite.h"

#ifndef PLAYER_H
#define PLAYER_H
class Player : public Entity {
public:
  /// @brief  Player Object constructor.
  ///
  /// @param InExplosionSprite  Explosion Sprite object
  /// @param PlayerSprite  Sprite object
  /// @param InitY  Initial Y coordinate
  /// @param InitX  Inital X coordinate
  /// @param InitHealth  Initial Player object Health
  Player(Sprite InExplosionSprite, Sprite PlayerSprite,
         const std::size_t &InitY, const std::size_t &InitX, int InitHealth);

  /// @brief  Player health getter
  ///
  /// @returns  Health
  int getHealth();

  /// @brief  Evaluates whenever InBullet object hit the Player object or not
  ///
  /// @param InBullet Reference to Bullet object
  ///
  /// @returns  true on hit otherwise false
  bool hit(Bullet &InBullet);

  /// @brief  Evaluates if Player object was hit or not.
  ///
  /// @returns  true if its hit otherwise false
  bool isHit();

  /// @brief  Health setter
  ///
  /// @param InHealth New Player Health value
  void setHealth(const int &InHealth);

  /// @brief  Player score setter
  ///
  /// @param InScore New Player Score value
  void setScore(const int &InScore);

  /// @brief  Player score getter
  ///
  /// @returns  Player Score value
  unsigned int getScore();

  /// @brief  Increments the player score with value of "Increment"
  ///
  /// @param Increment Increment value
  void incrementScore(unsigned int Increment);

  /// @brief  Explosion Sprite object getter.
  ///
  /// @returns  returns "InExplosionSprite" provided during construction
  Sprite getExplosionSprite();

private:
  unsigned int Score;
  Sprite ExplosionSprite;
  bool Exploded;
  int Health;
  bool Shot;
};
#endif
