/// @file
/// @brief Assets header file
#include <array>
#include <cstdint>

#ifndef ASSET_H
#define ASSET_H

#define ALIENA_WIDTH 24
#define ALIENA_SCORE 10
#define ALIENB_WIDTH 22
#define ALIENB_SCORE 20
#define ALIENC_WIDTH 16
#define ALIENC_SCORE 30
#define ALIEN_EXPLODING_WIDTH 27
#define PLAYER_WIDTH 32
#define PLAYER_SHOT_WIDTH 2
#define PLAYER_SHOT_EXP_WIDTH 16
#define ALIEN_EXP_WIDTH 32
#define ALIEN_SHOT_WIDTH 6
#define ALIEN_SHOT_EXP_WIDTH 12
#define SHIELD_WIDTH 44
#define FLYING_SAUCER_WIDTH 48

extern const std::array<uint32_t, 16> AlienA;
extern const std::array<uint32_t, 16> AlienB;
extern const std::array<uint32_t, 16> AlienC;
extern const std::array<uint32_t, 16> AlienAMoving;
extern const std::array<uint32_t, 16> AlienBMoving;
extern const std::array<uint32_t, 16> AlienCMoving;
extern const std::array<uint32_t, 16> PlayerSprite;
extern const std::array<uint32_t, 16> PlayerBlowupSprite1;
extern const std::array<uint32_t, 16> PlayerBlowupSprite2;
extern const std::array<uint8_t, 8> PlayerShotSprite;
extern const std::array<uint32_t, 16> PlayerShotExploding;
extern const std::array<uint32_t, 16> AlienExplodingSprite;
extern const std::array<uint8_t, 16> SquiglyShotSprite1;
extern const std::array<uint8_t, 16> SquiglyShotSprite2;
extern const std::array<uint8_t, 16> SquiglyShotSprite3;
extern const std::array<uint16_t, 16> AlienShotExploding;
extern const std::array<uint8_t, 16> PlungerShotSpite1;
extern const std::array<uint8_t, 16> PlungerShotSpite2;
extern const std::array<uint8_t, 16> PlungerShotSpite3;
extern const std::array<uint8_t, 16> PlungerShotSpite4;
extern const std::array<uint8_t, 16> RollingShotSprite1;
extern const std::array<uint8_t, 16> RollingShotSprite2;
extern const std::array<uint8_t, 16> RollingShotSprite3;
extern const std::array<uint64_t, 32> ShieldImage;
extern const std::array<uint64_t, 16> FlyingSaucerSprite;
extern const std::array<uint64_t, 16> SpriteSaucerExp;
#endif
