/// @file
/// @brief Bunker module header file

#include "Bullet.h"
#include "Entity.h"
#include "Sprite.h"

#ifndef BUNKER_H
#define BUNKER_H
class Bunker : public Entity {
public:
  /// @brief Bunker object constructor
  ///
  /// @param InitSprite Sprite object
  /// @param InitY Initial Y coordinate
  /// @param InitX Initial X coordinate
  Bunker(Sprite InitSprite, const int &InitY, const int &InitX);

  /// @brief Evaluates whenever given Bullet object hit the given Bunker
  /// object. Damages sprite accordingly.
  ///
  /// @param InBullet Reference to Bullet object
  ///
  /// @returns true when InBullet hit the Bunker object otherwise false
  bool hit(Bullet &InBullet);
};

#endif
