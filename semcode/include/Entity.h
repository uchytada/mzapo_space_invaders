/// @file
/// @brief Entity module header file

#include "Sprite.h"

#ifndef ENTITY_H
#define ENTITY_H
class Entity {
public:
  /// @brief  Entity object constructor
  ///
  /// @param InitSprite  Sprite object
  /// @param InitY  Intial Y coordinate
  /// @param InitX  Intial X coordinate
  Entity(Sprite InitSprite, const int &InitY, const int &InitX);
  /// @brief  X coordinate getter
  ///
  /// @returns  X coordinate
  int getX();

  /// @brief  Y coordinate getter
  ///
  /// @returns  Y coordinate
  int getY();

  /// @brief  X coordinate getter
  ///
  /// @returns  X coordinate
  int getX() const;

  /// @brief  Y coordinate getter
  ///
  /// @returns  Y coordinate
  int getY() const;

  /// @brief  Reference to Sprite getter
  ///
  /// @returns  Reference to Sprite object
  Sprite &getSprite();

  /// @brief  Sprite object getter
  ///
  /// @returns  Sprite object
  Sprite getSprite() const;

  /// @brief  Y coordinate setter
  ///
  /// @param NewY New Y coordinate
  void setY(const int &NewY);

  /// @brief  X coordinate setter
  ///
  /// @param NewX New X coordinate
  void setX(const int &NewX);

private:
  int X;
  int Y;
  Sprite EntitySprite;
};
#endif
