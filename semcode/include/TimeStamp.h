/// @file
/// @brief Timestamp module header file

#include <sys/time.h>

#ifndef TIME_STAMP_H
#define TIME_STAMP_H
/// @brief calculates current timesstamp
///
/// @return current timestamp in ms
long long currentTimestamp();
#endif
