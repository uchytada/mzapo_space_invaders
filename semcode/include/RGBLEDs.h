/// @file
/// @brief Enemy module header file
#include <cstdint>
#include <cstdlib>
#include <vector>

#ifndef RGB_LEDS_H
#define RGB_LEDS_H

class RGBLED {

public:
  /// @brief  RGBLed object constructor.
  ///
  /// @param InLedNum Number of LED (0 - 1)
  /// @param InRed  Intial LED color (Red component) range: 0 - 255.
  /// @param InGreen  Initial LED color (Green component) range: 0 - 255.
  /// @param InBlue  Initial LED color (Blue component) range: 0 - 255.
  RGBLED(const uint8_t &InLedNum, const uint8_t &InRed, const uint8_t &InGreen,
         const uint8_t &InBlue);

  /// @brief  RGBLed object color setter.
  ///
  /// @param InRed  Red componenet range: 0 - 255.
  /// @param InGreen  Green component range: 0 - 255
  /// @param InBlue  Blue compoenet range: 0 - 255
  void setColor(const uint8_t &InRed, const uint8_t &InGreen,
                const uint8_t &InBlue);

  /// @brief  Flips the LED with a given period (Should be called in cycle).
  ///
  /// @param Period LED flipping period in ms.
  void blink(const uint32_t &Period);

private:
  long long t1;
  std::size_t ActualCycle;
  unsigned char *MemBase;
  unsigned char *MemLed;
  bool FlagLed;
  uint8_t Red;
  uint8_t Green;
  uint8_t Blue;
  void flip();
};

#endif
