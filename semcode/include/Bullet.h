/// @file
/// @brief Bullet module header file

#include "Entity.h"
#include "Sprite.h"

#ifndef BULLET_H
#define BULLET_H
typedef enum { Up, Down } Direction;

typedef enum { EnemyOrigin, PlayerOrigin } Origin;

class Bullet : public Entity {
public:
  /// @brief  Bullet object constructor
  ///
  /// @param InitSprite Bullet Sprite object
  /// @param InitY  Initial Y coords
  /// @param InitX  Initial X coords
  /// @param InOrigin  Bullet Origin
  /// @param InDirection Bullet Direction
  Bullet(Sprite InitSprite, const int &InitY, const int &InitX,
         const Origin &InOrigin, const Direction &InDirection);

  /// @brief  Bullet origin getter
  ///
  /// @returns  "InOrigin" provided during construction.
  Origin getOrigin();

  /// @brief  Bullet direction getter
  ///
  /// @returns  "InDirection" provided during construction
  Direction getDirection();

private:
  Direction BulletDirection;
  Origin BulletOrigin;
};
#endif
