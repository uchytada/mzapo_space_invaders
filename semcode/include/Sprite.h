/// @file
/// @brief Entity module header file

#include <vector>

#include "Display.h"

#ifndef SPRITE_H
#define SPRITE_H

typedef enum { DamageUp, DamageDown } DamageDirection;
typedef std::vector<std::vector<Pixel>> PixelMatrix;
class Sprite {
public:
  /// @brief  Sprite object constructor
  ///
  /// @param Asset  Asset provided by Assets.cpp
  /// @param AssetWidth Asset width defined in Assets.h
  Sprite(const std::array<uint32_t, 16> &Asset, const int AssetWidth);

  /// @brief  Sprite object constructor
  ///
  /// @param Asset  Asset provided by Assets.cpp
  /// @param AssetWidth  Asset width defined in Assets.h
  Sprite(const std::array<uint64_t, 32> &Asset, const int AssetWidth);

  /// @brief  Sprite object constructor
  ///
  /// @param Asset  Asset provided by Assets.cpp
  /// @param AssetWidth  Asset width defined in Assets.h
  Sprite(const std::array<uint8_t, 8> &Asset, const int AssetWidth);

  /// @brief  Sets the color of all non-black Pixels inside the Sprite object
  ///
  /// @param Red  Pixel red component (range 0 - 31)
  /// @param Green Pixel green component (range 0 - 31)
  /// @param Blue  Pixel blue component (range 0 - 31)
  void setColor(const int &Red, const int &Green, const int &Blue);

  /// @brief  Getter for Pixel on X, Y coordiantes.
  ///
  /// @param X X coordinate
  /// @param Y Y coordinate
  ///
  /// @returns  Pixel on X, Y coordients, when X and Y is in range. Otherwise
  /// black Pixel.
  Pixel getPixel(const int &X, const int &Y);

  /// @brief  Getter for Pixel on X, Y coordiantes.
  ///
  /// @param X X coordinate
  /// @param Y Y coordinate
  ///
  /// @returns  Pixel on X, Y coordients, when X and Y is in range. Otherwise
  /// black Pixel.
  Pixel getPixel(const int &X, const int &Y) const;

  /// @brief  Sprite Width getter
  ///
  /// @returns  Width of the Sprite object
  int getWidth();

  /// @brief  Sprite Height getter
  ///
  /// @returns  Height of the Sprite object
  int getHeight();

  /// @brief Sprite Width getter
  ///
  /// @returns  Width of the Sprite object
  int getWidth() const;

  /// @brief  Sprite Height getter
  ///
  /// @returns  Height of the Sprite object
  int getHeight() const;

  /// @brief  Damages the Sprite object according to Radius and DamageDirection.
  ///
  /// @param Y  Y coordinate of the damage
  /// @param X  X coordinate of the damage
  /// @param Radius Radius of the damage
  /// @param InDamageDirection Direction of the damage
  ///
  /// @returns  true if Sprite got damaged otherwise false
  bool damage(int Y, int X, const int &Radius,
              const DamageDirection &InDamageDirection);

private:
  int SpriteWidth;
  int SpriteHeight;
  PixelMatrix Pixels;
};

void drawSprite(Grid &Pixels, int SpriteY, int SpriteX, const Sprite &InSprite);
#endif
