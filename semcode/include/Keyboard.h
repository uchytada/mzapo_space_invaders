/// @file
/// @brief Keyboard module header

#ifndef KEYBOARD_H
#define KEYBOARD_H

/// @brief Returns value according to STDIN input
///
/// @returns 1 if 'a' was pressed,
///          -1 if 'd' was pressed,
///          0 if ' ' was pressed,
///          100 otherwise
int keyboard();

/// @brief  Sets terminal to RAW mode. This function was taken from B3B36PRG
/// semestral project.
///
/// @param reset if 0 terminal is set to RAW mode and previous settings are
/// stored. if 1 previous settings are restored.
void call_termios(int reset);

/// @brief
///
/// @param fd  IO file dectriptor.
/// @param timeout_ms  Polling timeout in ms.
/// @param c  Pointer to char (output).
///
/// @returns 0 on timeout,
///          1 on succesfull read,
///          -1 on error
int io_getc_timeout(int fd, int timeout_ms, unsigned char *c);
#endif
