/// @file
/// @brief Enemy module header file

#include "Entity.h"
#include "Sprite.h"

#ifndef ENEMY_H
#define ENEMY_H
typedef enum { MovingLeft, MovingRight, Descending, StateCount } EnemyState;

class Enemy : public Entity {
public:
  /// @brief  Enemy object constructor
  ///
  /// @param InScoreGain  Score for shooting down this Enemy
  /// @param InitExpSprite  Explosion Sprite object
  /// @param InitMovingSprite  Moving Sprite object
  /// @param InitSprite  Sprite Object
  /// @param InColumn Object  Column position
  /// @param InitY Initial Y coord
  /// @param InitX Inital X coord
  Enemy(unsigned int InScoreGain, Sprite InitExpSprite, Sprite InitMovingSprite,
        Sprite InitSprite, const int &InColumn, const int &InitY,
        const int &InitX);

  /// @brief Asks whenever Enemy exploded and thus is ready to be erased
  ///
  /// @returns  true if Enemy object exploaded otherwise false.
  bool exploaded();

  /// @brief  Explosion setter
  void setExploded();

  /// @brief Explosion sprite getter
  ///
  /// @returns  Sprite object "InitExpSprite" provided during constuction
  Sprite getExplosionSprite();

  /// @brief  Moving state getter that also changes the state
  ///
  /// @returns  Enemy object moving state
  EnemyState getState();

  /// @brief  Enemy moving state setter
  ///
  /// @param New moving state
  void setState(const EnemyState &InState);

  /// @brief  Column getter
  ///
  /// @returns  Column numver "InColumn" provided during construction
  int getColumn();

  /// @brief Simulates movement
  ///
  /// @returns  Alternately returns "InitMovingSprit" and "InitSpite" Sprite
  /// objects provided during construction
  Sprite getMovingSprite();

  /// @brief  Score gain getter
  ///
  /// @returns  "InScoreGain" value provided during construction;
  unsigned int getScoreGain();

private:
  unsigned int scoreGain;
  int Column;
  int Cursor = 8;
  int MaxCursor = 16;
  bool Exploaded;
  bool Moving = false;
  Sprite ExplosionSprite;
  EnemyState State;
  Sprite MovingSprite;
};

#endif
