///@file
///@brief Game module header file
#include <vector>

#include "Assets.h"
#include "Bullet.h"
#include "Bunker.h"
#include "Enemy.h"
#include "Knobs.h"
#include "Player.h"

#ifndef GAME_H
#define GAME_H

typedef enum { StateMenu, StateInit, StateGameLoop, StateGameOver } GameState;
/// @brief  Iterates trough all the Enemy objects and evalutes whenever
/// Bullet object represented by BulletIter hit any enemy. Also sets the score
/// of Player object accordingly.
///
/// @param InEnemies  Vector of Enemy objects
/// @param BulletIter  Iterator of vector of Bullet objects
/// @param InPlayer Player Object
///
/// @returns true if hit otherwise false
bool enemyHit(std::vector<Enemy> &InEnemies,
              std::vector<Bullet>::iterator BulletIter, Player &InPlayer);

/// @brief Iterates rought all the Bunker objects nad evaluates whenever
/// Bullet object represented by BulletIter hit any Bunker. Damages Bunker
/// object Accordingly.
///
/// @param InBunkers  Vector of Bunker Objects
/// @param BulletIter  Iterator of vector of Bullet objects
///
/// @returns  true if hit otherwise false
bool bunkerHit(std::vector<Bunker> &InBunkers,
               std::vector<Bullet>::iterator BulletIter);

/// @brief  Determines whenever Bullet Object hit the Player Object or not.
/// Sets the Playerobject health accrodingly
///
/// @param InPlayer Player Object
/// @param BulletIter Iterator of vector of Bullet objects
///
/// @returns
bool playerHit(Player &InPlayer, std::vector<Bullet>::iterator BulletIter);

/// @brief  Sets all the pixels of a given Grid object to black
///
/// @param Pixels reference to grid of Pixels
void clearPixelGrid(Grid &Pixels);

/// @brief  Draws all Bunker objects from vector of Bunker objects into grid of
/// Pixels
///
/// @param Pixels Grid of Pixels
/// @param Bunkers Vector of bunkers
void drawBunkers(Grid &Pixels, const std::vector<Bunker> &Bunkers);

/// @brief  Intializes the vector of Enemy object to Space Invaders
///
/// @param InEnemies  Vector of Enemy Objects
void initEnemies(std::vector<Enemy> &InEnemies);

/// @brief  Handles the Bullets part of game loop.
///
/// @param InBullets Vector of Bullet objects
/// @param InEnemies Vector of Enemy objects
/// @param Bunkers  Vector of Bunker objects
/// @param InPlayer Player object
/// @param InPixels Grid of Pixels
void resolveBullets(std::vector<Bullet> &InBullets,
                    std::vector<Enemy> &InEnemies, std::vector<Bunker> &Bunkers,
                    Player &InPlayer, Grid &InPixels);

/// @brief  Hadles the Enemy part of the game loop
///
/// @param InEnemies Vector of Enemy objects
/// @param InBullets Vector of Bullet objects
/// @param InPixels  Grid of Pixels
/// @param InGameOver GameOver boolean.
/// @param ShootFrequency Number determining the probability of Enemy object
/// shooting a Bullet object. Lower number means higher chance.
void resolveEnemies(std::vector<Enemy> &InEnemies,
                    std::vector<Bullet> &InBullets, Grid &InPixels,
                    bool &InGameOver, const int &InShootFrequency);

/// @brief  Handles the Player part of the game loop
///
/// @param InPlayer  Player object
/// @param InBullets  Vector of Bullet objects
/// @param InKnob  Control Knob object
/// @param InPixels Grid of Pixels
/// @param InKeyboard  Keyboard boolean (Switching input from Knob to
/// Keyboard)
void resolvePlayer(Player &InPlayer, std::vector<Bullet> &InBullets,
                   Knob &InKnob, Grid &InPixels, const bool &InKeyboard);
#endif
