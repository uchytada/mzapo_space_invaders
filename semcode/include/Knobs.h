/// @file
/// @brief Knobs module header file

#ifndef KNOB_H
#define KNOB_H
class Knob {
public:
  /// @brief  Knob object constructor
  ///
  /// @param InNumberKnob Number of Knob on MZ_APO board (0 - 2)
  Knob(uint8_t InNumberKnob);

  /// @brief  Reads the current value from appropriate Knob register compares it
  /// with last value and creates increment
  ///
  /// @returns increment from previous read in interval (-100, 100)
  int8_t increment();

  /// @brief  Checks whenever Knob is pressed or not
  ///
  /// @returns  true if Knob is pressed otherwise false.
  bool button();

  /// @brief Set object to ignore button press for certain time
  ///
  /// @param InMS Ignore time in ms
  void ignoreButtonMS(unsigned int InMS);

private:
  unsigned char *MemBase;
  long long IgnoreTime;
  bool IgnoreButton;
  uint8_t Last;
  uint8_t NumberKnob;
};
#endif
