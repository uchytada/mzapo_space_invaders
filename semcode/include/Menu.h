/// @file
/// @brief Menu module header file

#include <string>

#include "Display.h"
#include "Text.h"

#ifndef MENU_H
#define MENU_H

class Menu {

public:
  /// @brief  Menu object constructor
  ///
  /// @param  InPixelGrid Reference to Grid of pixels
  Menu(Grid &InPixelGrid);

  /// @brief  Selects the appropriate Menu item
  ///
  /// @param  Increment Increment (return by Knob increment() method)
  /// @param  Button Button state (returnted by Knob button() method)
  ///
  /// @returns  On button press it returns mumber of selected menu item
  /// otherwise 0
  unsigned int setOptions(int8_t Increment, bool Button);

  /// @brief  Select the appropriate Menu item
  ///
  /// @param KeyboardRet  Return from keyboard() function
  ///
  /// @returns On button press it returns mumber of selected menu item otherwise
  /// 0
  unsigned int setOptionsKeyboard(const int &KeyboardRet);

private:
  Grid &PixelGrid;
  int Cursor = 0;
  uint8_t Style = 1;
  const std::string StrTitle = "Space Invaders";
  uint32_t TitlePosX = 200;
  uint32_t TitlePosY = 100;
  uint8_t TitleColor[3] = {0, 31, 0};
  int8_t CountOpt = 4;
  std::string Option[4] = {"EASY", "NORMAL", "HARD", "EXIT"};
  uint32_t OptPosX[4] = {0, 0};
  uint32_t OptPosY[4] = {160, 180, 200, 220};
  uint8_t DefColor[3] = {31, 31, 31};
  uint8_t Highlight[3] = {31, 0, 0};
};

#endif
