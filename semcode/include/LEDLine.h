/// @file
/// @brief LEDLine module header file

#include <cstdlib>
#include <ctime>

#ifndef LED_LINE_H
#define LED_LINE_H

class LEDLine {

public:
  /// @brief LEDLine object constructor.
  ///
  /// @param InPeriod Period (of LEDLine shifting).
  LEDLine(const uint32_t &InPeriod);

  /// @brief  Shifts the LEDLine according to period. (Should be called in
  /// cycle).
  void ledShift();

private:
  unsigned char *MemBase;
  bool FlagLED;
  uint8_t ActualLED;
  uint32_t LEDBits;
  uint32_t Period;
  long long t1;
  std::size_t ActualCycle;
};

#endif
