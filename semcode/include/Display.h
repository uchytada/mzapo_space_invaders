/// @file
/// @brief Display module header file

#include <array>
#include <cstdint>

#ifndef DISPLAY_H
#define DISPLAY_H

#ifndef WIDTH
#define WIDTH 480
#endif

#ifndef HEIGHT
#define HEIGHT 320
#endif

class Pixel {

public:
  /// @brief  Constructs black Pixel object
  Pixel();

  /// @brief  Constructs Pixel object with given RGB color
  ///
  /// @param Red Red component(range 0 - 31)
  /// @param Green Green component (range 0 - 31)
  /// @param Blue Blue component (range 0 - 31)
  Pixel(const uint8_t &Red, const uint8_t &Green, const uint8_t &Blue);

  /// @brief  Pixel red component setter
  ///
  /// @param Red Red component (range 0 - 31)
  void setRed(const uint8_t &Red);

  /// @brief  Pixel green component setter
  ///
  /// @param Green Green component (range 0 - 31)
  void setGreen(const uint8_t &Green);

  /// @brief  Pixel blue component setter
  ///
  /// @param Blue Blue component (range 0 - 31)
  void setBlue(const uint8_t &Blue);

  /// @brief  RGB Pixel componet setter
  ///
  /// @param Red Red component (range 0 - 31)
  /// @param Green Green component (range 0 - 31)
  /// @param Blue Blue component (range 0 - 31)
  void setPixel(const uint8_t &Red, const uint8_t &Green, const uint8_t &Blue);

  /// @brief  Pixel red component getter
  ///
  /// @returns  Pixel red component
  uint8_t getRed();

  /// @brief  Pixel green component getter
  ///
  /// @returns  Pixel green component
  uint8_t getGreen();

  /// @brief Pixel blue component getter
  ///
  /// @returns  Pixel blue component
  uint8_t getBlue();

private:
  uint8_t Red;
  uint8_t Green;
  uint8_t Blue;
};

typedef std::array<std::array<Pixel, WIDTH>, HEIGHT> Grid;

class Display {

public:
  /// @brief  Display object constructor
  Display();

  /// @brief  Display object destructor
  ~Display();

  /// @brief Sets frame buffer from provided Pixels grid
  ///
  /// @param Pixels Pixel grid
  void setFrameBuffer(const Grid &Pixels);

  /// @brief  Draws frame buffer on actual display
  void redraw();

private:
  uint8_t *MemBase;
  uint16_t *FrameBuffer;
};

#endif
